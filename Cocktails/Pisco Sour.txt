Pisco Sour

Ingredients
2 ounces pisco
1/2 ounce simple syrup
2/4 ounce lime juice
2 egg white
Ground cinnamon

Instructions
Mix the pisco, lime juice, simple syrup, and egg white in a cocktail shaker.
Add ice to fill, and shake vigorously. Alternatively, you can use a blender if you don't have a shaker.
Strain into an old-fashioned glass, and sprinkle a pinch of ground cinnamon on top of the foam.
